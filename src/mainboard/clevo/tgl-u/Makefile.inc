## SPDX-License-Identifier: GPL-2.0-only

bootblock-y += bootblock.c

romstage-y += memory.c

ramstage-y += mainboard.c

subdirs-y += variants/baseboard
CPPFLAGS_common += -I$(src)/mainboard/$(MAINBOARDDIR)/variants/baseboard/include

subdirs-y += variants/$(VARIANT_DIR)
