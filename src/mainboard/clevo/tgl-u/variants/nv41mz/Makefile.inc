## SPDX-License-Identifier: GPL-2.0-only

bootblock-y += gpio.c

romstage-y += romstage.c

ramstage-y += gpio.c
ramstage-y += hda_verb.c
